package com.marriana.aplikasimenu;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    //Inisialisasi Pertama di Java
    RelativeLayout profil;
    RelativeLayout kota;
    RelativeLayout edu;
    RelativeLayout family;
    RelativeLayout main;
    RelativeLayout keluar;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inisialisasi Kedua di Java
        profil = findViewById(R.id.profil);
        kota = findViewById(R.id.MyCity);
        edu = findViewById(R.id.MyEducation);
        family = findViewById(R.id.MyFamily);
        main = findViewById(R.id.Quiz);
        keluar = findViewById(R.id.Exit);

        //Button Profil Ketika DiKlik
        profil.setOnClickListener(new View.OnClickListener()
        {
         @Override
         public void onClick(View v)
         {
         Toast.makeText(getApplicationContext(), "Profil Telah Dipilih", Toast.LENGTH_SHORT).show();
         Intent beach = new Intent(MainActivity.this, Myprofil.class);
          startActivity(beach);
         }
         }
        );

        //Button MyCity Ketika DiKlik
        kota.setOnClickListener(new View.OnClickListener()
         {
       @Override
        public void onClick(View v)
      {
        Toast.makeText(getApplicationContext(), "MyCity Telah Dipilih", Toast.LENGTH_SHORT).show();
        Intent beach = new Intent(MainActivity.this, Mycity.class);
        startActivity(beach);
      }
        }
        );

        //Button MyEducation Ketika DiKlik
        edu.setOnClickListener(new View.OnClickListener()
       {
       @Override
       public void onClick(View v)
      {
      Toast.makeText(getApplicationContext(), "MyEducation Telah Dipilih", Toast.LENGTH_SHORT).show();
      Intent beach = new Intent(MainActivity.this, Myeducation.class);
      startActivity(beach);
      }
       });

        //Button MyFamily Ketika DiKlik
        family.setOnClickListener(new View.OnClickListener()
        {
          @Override
         public void onClick(View v)
         {
          Toast.makeText(getApplicationContext(), "MyFamily Telah Dipilih", Toast.LENGTH_SHORT).show();
          Intent beach = new Intent(MainActivity.this, Myfamily.class);
          startActivity(beach);
         }
        }
        );

        //Button Quiz Ketika DiKlik
        main.setOnClickListener(new View.OnClickListener()
         {
         @Override
        public void onClick(View v)
        {
        Toast.makeText(getApplicationContext(), "Quiz Telah Dipilih", Toast.LENGTH_SHORT).show();
        Intent beach = new Intent(MainActivity.this, Quiz.class);
        startActivity(beach);
        }
        }
        );

        //Button Exit Ketika DiKlik
        keluar.setOnClickListener(new View.OnClickListener()
        {
        @Override
        public void onClick(View v)
          {
          Toast.makeText(getApplicationContext(), "Aplikasi Keluar", Toast.LENGTH_SHORT).show();
         finish();
        System.exit(0);
        }
        }
        );


    }

    @Override
    public void onBackPressed() {
    new AlertDialog.Builder(this)
     .setIcon(android.R.drawable.ic_dialog_alert)
      .setTitle("Closing Activity")
      .setMessage("Are you sure you want to close this activity?")
      .setPositiveButton("Yes", new DialogInterface.OnClickListener()
       {
       @Override
        public void onClick(DialogInterface dialog, int which) {
        finish();
        }

        })
       .setNegativeButton("No", null)
       .show();

    }

}