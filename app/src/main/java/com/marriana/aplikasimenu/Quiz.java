package com.marriana.aplikasimenu;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;


public class Quiz  extends AppCompatActivity {

    EditText ed1;
    TextView tv1,tv2,tv3;
    RadioButton a,b,c,d;
    Button bt;
    RadioGroup rg;
    int q,s;
    ImageView image_senyum,image_sedih,image_marah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activityquiz);
        ed1=(EditText) findViewById(R.id.name);
        tv1=(TextView)findViewById(R.id.ques);
        tv2=(TextView)findViewById(R.id.response);
        tv3=(TextView)findViewById(R.id.score);
        rg=(RadioGroup)findViewById(R.id.optionGroup);
        a=(RadioButton)findViewById(R.id.option1);
        b=(RadioButton)findViewById(R.id.option2);
        c=(RadioButton)findViewById(R.id.option3);
        d=(RadioButton)findViewById(R.id.option4);
        bt=(Button)findViewById(R.id.next);
        image_senyum = (ImageView) findViewById(R.id.emotsenyum);
        image_sedih = (ImageView) findViewById(R.id.emotsedih);
        image_marah = (ImageView) findViewById(R.id.emotmarah);
        q=0;
        s=0;

    }
    public void quiz(View v){
        switch (q){
            case 0:
            {
                rg.setVisibility(View.VISIBLE);
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);

                tv2.setText("");
                tv3.setText("");

                a.setEnabled(true);
                b.setEnabled(true);
                c.setEnabled(true);
                d.setEnabled(true);
                ed1.setEnabled(true);
                bt.setText("Next");
                s=0;

                tv1.setText("1.Apa Bahasa Inggris Burung =");
                a.setText("Bird");
                b.setText("Cat");
                c.setText("Dog");
                d.setText("Snake");
                q=1;
                break;
            }
            case 1:
            {
                ed1.setVisibility(View.INVISIBLE);
                ed1.setEnabled(false);
                tv1.setText("2.26-2=");
                a.setText("12");
                b.setText("26");
                c.setText("24");
                d.setText("28");

                if (a.isChecked())
                {
                    //Memanggil Fungsi JawabBenar(); ada dibawah
                    JawabBenar();
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    //Memanggil Fungsi TidakDiJawab(); ada dibawah
                    TidakDiJawab();
                }
                else if (!a.isChecked())
                {
                    //Memanggil Fungsi SalahJawab(); ada dibawah
                    SalahJawab();
                }
                q=2;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }

            case 2:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("3.Siapakah Presiden pertama Republik Indonesia?");
                a.setText("Presiden");
                b.setText("Soekarno");
                c.setText("Soeharto");
                d.setText("Bapak gue");


                if (c.isChecked())
                {
                    //Memanggil Fungsi JawabBenar(); ada dibawah
                    JawabBenar();
                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    //Memanggil Fungsi TidakDiJawab(); ada dibawah
                    TidakDiJawab();
                }
                else if (!c.isChecked())
                {
                    //Memanggil Fungsi SalahJawab(); ada dibawah
                    SalahJawab();
                }
                q=3;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }


            case 3:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("4. Siapa Penemu Telepon ? ");
                a.setText("Mariana");
                b.setText("Ayu");
                c.setText("Nisa");
                d.setText("Alex Sander Grahamber");

                if (b.isChecked())
                {
                    JawabBenar();

                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    TidakDiJawab();
                }
                else if (!b.isChecked())
                {
                    SalahJawab();
                }
                q=4;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }

            case 4:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("5.Pada Tahun Berapa Indoensia Merdeka?");
                a.setText("1944");
                b.setText("1969");
                c.setText("1954");
                d.setText("1945");


                if (d.isChecked())
                {
                    JawabBenar();

                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    TidakDiJawab();

                }
                else if (!d.isChecked())
                {
                    SalahJawab();
                }
                q=5;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }

            case 5:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("6.2+2?");
                a.setText("1");
                b.setText("5");
                c.setText("4");
                d.setText("3 ");


                if (d.isChecked())
                {
                    JawabBenar();

                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    TidakDiJawab();

                }
                else if (!d.isChecked())
                {
                    SalahJawab();
                }
                q=6;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 6:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("7. Kita Berjalan Menggunakan ?");
                a.setText("Kaki");
                b.setText("Tangan");
                c.setText("Kepala");
                d.setText("Mata");


                if (c.isChecked())
                {
                    JawabBenar();

                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    TidakDiJawab();

                }
                else if (!c.isChecked())
                {
                    SalahJawab();
                }
                q=7;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 7:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("5.Ayah Menggunakan Radio menggunakan ?");
                a.setText("Mulut");
                b.setText("Hidung");
                c.setText("Telinga");
                d.setText("Semua Salah, yang benar hanya Dia");


                if (a.isChecked())
                {
                    JawabBenar();

                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    TidakDiJawab();

                }
                else if (!a.isChecked())
                {
                    SalahJawab();
                }
                q=8;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 8:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("5.Ibukota Indonesia adalah?");
                a.setText("Lampung");
                b.setText("Jakarta");
                c.setText("Jambi");
                d.setText("Medan");


                if (c.isChecked())
                {
                    JawabBenar();

                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    TidakDiJawab();

                }
                else if (!c.isChecked())
                {
                    SalahJawab();
                }
                q=9;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 9:
            {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("150-25= ?");
                a.setText("81");
                b.setText("83");
                c.setText("85");
                d.setText("125");


                if (b.isChecked())
                {
                    JawabBenar();

                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    TidakDiJawab();

                }
                else if (!b.isChecked())
                {
                    SalahJawab();
                }
                q=10;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 10:
            {
                ed1.setVisibility(View.INVISIBLE);
                a.setEnabled(false);
                b.setEnabled(false);
                c.setEnabled(false);
                d.setEnabled(false);
                if (d.isChecked())
                {
                    JawabBenar();

                }
                else if (!a.isChecked()&&!b.isChecked()&&!c.isChecked()&&!d.isChecked())
                {
                    TidakDiJawab();
                }
                else if (!d.isChecked())
                {
                    SalahJawab();
                }
                tv3.setText(ed1.getText()+"'s final score is "+s);
                bt.setText("Restart");
                q=0;
                break;
            }
        }
    }
    public void JawabBenar()
    {
        tv2.setText("Jawaban Benar");
        s=s+10;
        image_senyum.setVisibility(View.VISIBLE);
        image_senyum.postDelayed(new Runnable() {
            @Override
            public void run() {
                image_senyum.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }

    //Buat 1 Class Biar Tinggal Dipanggil Saja Jika Jawaban Benar
    public void TidakDiJawab()
    {
        tv2.setText("Mohon Maaf Jawaban Tidak Dipilih");
        s=s+0;
        image_marah.setVisibility(View.VISIBLE);
        image_marah.postDelayed(new Runnable() {
            @Override
            public void run() {
                image_marah.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }

    //Buat 1 Class Biar Tinggal Dipanggil Saja Jika Jawaban Benar
    public void SalahJawab()
    {
        tv2.setText("Mohon Maaf Jawaban Salah");
        s=s-5;
        image_sedih.setVisibility(View.VISIBLE);
        image_sedih.postDelayed(new Runnable() {
            @Override
            public void run() {
                image_sedih.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }
}